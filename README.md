# Plan UZ

Plan UZ is an application for browsing students', teachers' and rooms' schedule written for University of Zielona Góra.

## Getting Started

To build this app, you will need Android Studio. Clone the repository using:

`git clone https://gitlab.com/plan-uz/plan-uz-android`

Next you will need to open it using Android Studio, sync project with Gradle files and you're good to go.

## Dependencies

* [NiceSpinner](https://github.com/arcadefire/nice-spinner) - a re-implementation of the default Android's spinner, with a nice arrow animation and a different way to display its content. We can use the newest version available here.
* [FlexboxLayout 1.0.0](https://github.com/google/flexbox-layout) - a library project which brings the similar capabilities of CSS Flexible Box Layout Module to Android. We're using version 1.0.0, because we haven't migrated to AndroidX.

You don't need to download anything, Android Studio will do it for you once you sync your project.

## Authors

* **Axel Rechenberg** - *Android Application* - [Axel Rechenberg](https://gitlab.com/axelrechenberg)
* **Mateusz Kita** - *Schedule Scraper* - [Mateusz Kita](https://gitlab.com/MateuszKita)
* **Jarosław Rauza** - *Schedule Scraper* - [Jarosław Rauza](https://gitlab.com/EvilusPL)
* **Jakub Miechowicz** - *Firebase Integration*