package com.planuz;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.planuz.scraper.scrapers.courses.CoursesScraper;
import com.planuz.scraper.scrapers.groups.GroupsScraper;

import org.angmarch.views.NiceSpinner;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Main menu activity. Shows the user courses, groups and subgroups and allows to choose one of each.
 * Starts the schedule activity.
 */
public class MainMenuActivity extends AppActivity {
    /**
     * Fills the spinners with data.
     */
    private GroupsScraper groupsScraper;
    public URI groupUri;
    public String subgroup;

    private void fillSpinners() {
        final NiceSpinner courseSpinner = findViewById(R.id.courseSpinner);
        final NiceSpinner groupSpinner = findViewById(R.id.groupSpinner);
        final NiceSpinner subgroupSpinner = findViewById(R.id.subgroupSpinner);
        final Button buttonApply = findViewById(R.id.button_apply);
        final CoursesScraper coursesScraper = new CoursesScraper();
        final int grayColor = Color.rgb(220, 220, 220);
        final int whiteColor = Color.rgb(255, 255, 255);
        final int primaryGreen = Color.rgb(196, 222, 160);
        final Context thisContext = this;

        groupSpinner.setEnabled(false);
        subgroupSpinner.setEnabled(false);
        buttonApply.setEnabled(false);
        groupSpinner.setBackgroundColor(grayColor);
        subgroupSpinner.setBackgroundColor(grayColor);
        buttonApply.setBackgroundColor(grayColor);


        coursesScraper.setCourseSpinner(courseSpinner);
        courseSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                subgroupSpinner.setEnabled(false);
                subgroupSpinner.setBackgroundColor(grayColor);
                buttonApply.setEnabled(false);
                buttonApply.setBackgroundColor(grayColor);
                groupSpinner.setSelectedIndex(0);
                subgroupSpinner.setSelectedIndex(0);
                if (courseSpinner.getSelectedIndex() > 0) {
                    groupSpinner.setEnabled(true);
                    groupSpinner.setBackgroundColor(whiteColor);
                } else {
                    groupSpinner.setEnabled(false);
                    groupSpinner.setBackgroundColor(grayColor);
                    buttonApply.setEnabled(false);
                    buttonApply.setBackgroundColor(grayColor);
                }
                try {
                    if (courseSpinner.getSelectedIndex() > 0) {
                        groupsScraper =
                                new GroupsScraper(coursesScraper.getCourseUriByCourseIndex(courseSpinner.getSelectedIndex() - 1));
                        groupsScraper.setGroupSpinner(groupSpinner);
                        groupsScraper.execute(thisContext);
                    }
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        groupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                subgroupSpinner.setSelectedIndex(0);
                buttonApply.setEnabled(false);
                buttonApply.setBackgroundColor(grayColor);
                if (groupSpinner.getSelectedIndex() > 0) {
                    subgroupSpinner.setEnabled(true);
                    subgroupSpinner.setBackgroundColor(whiteColor);
                    try {
                        groupUri = groupsScraper.getGroupUriByGroupIndex(groupSpinner.getSelectedIndex() - 1);
                        System.out.println(groupUri.toString());
                        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!");
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                } else {
                    subgroupSpinner.setEnabled(false);
                    subgroupSpinner.setBackgroundColor(grayColor);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        subgroupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                switch (subgroupSpinner.getSelectedIndex()) {
                    case 0:
                        subgroup = "ALL";
                        break;
                    case 1:
                        subgroup = "A";
                        break;
                    case 2:
                        subgroup = "B";
                        break;
                    default:
                }
                if (subgroupSpinner.getSelectedIndex() > 0) {
                    buttonApply.setEnabled(true);
                    buttonApply.setBackgroundColor(primaryGreen);
                } else {
                    buttonApply.setEnabled(false);
                    buttonApply.setBackgroundColor(grayColor);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        String[] subgroups = new String[]{
                "Select subgroup",
                "A",
                "B"
        };
        List<String> subgroupsList = new ArrayList<>(Arrays.asList(subgroups));

        ArrayAdapter<String> subgroupSpinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, subgroupsList) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }
        };

        subgroupSpinner.setAdapter(subgroupSpinnerArrayAdapter);
        coursesScraper.execute(this);
    }

    /**
     * {inheritDoc}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        setupActionBar();
        fillSpinners();
    }

    /**
     * Starts the student schedule activity.
     *
     * @param view The element that caused this action.
     */
    public void showStudentSchedule(View view) {
        Intent intent = new Intent(this, StudentScheduleActivity.class);
        intent.putExtra("scheduleUri", groupUri.toString());
        intent.putExtra("subgroup", subgroup);
        startActivity(intent);
    }
}
