package com.planuz;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

/**
 * A loading screen that fetches data needed by the application.
 */
public class MainActivity extends AppCompatActivity {

    /**
     * Fetches schedule data and starts main menu once done.
     */
    private void loadData() {
        Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                startMainMenuActivity();
            }
        };
        handler.postDelayed(runnable, 1500);
    }

    /**
     * {inheritDoc}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * {inheritDoc}
     */
    @Override
    protected void onResume() {
        super.onResume();
        loadData();
    }

    /**
     * Starts main menu activity.
     */
    private void startMainMenuActivity() {
        Intent intent = new Intent(this, MainMenuActivity.class);
        startActivity(intent);
    }
}
