package com.planuz;

import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.planuz.scraper.models.GroupSchedule;
import com.planuz.scraper.scrapers.groupsSchedule.GroupsScheduleScraper;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Activity containing student schedule.
 */
public class StudentScheduleActivity extends AppActivity {

    private ArrayList<Button> buttons;
    private ArrayList<ArrayList<StudentScheduleRow>> studentScheduleDays;
    private ArrayList<GroupSchedule> groupSchedules;
    private TableLayout studentScheduleTable;
    private URI scheduleURI;
    private String subgroup;

    /**
     * Async task for fetching group schedule
     */
    private class GetGroupSchedule extends AsyncTask<URI, Void, Void> {

        @Override
        protected Void doInBackground(URI... uris) {

            GroupsScheduleScraper groupsScheduleScraper = new GroupsScheduleScraper();
            groupSchedules = groupsScheduleScraper.getGroupSchedule(uris[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            for (int i=0; i<groupSchedules.size(); i++) {
                if (subgroup.equals(Character.toString(groupSchedules.get(i).getSubgroup())) || groupSchedules.get(i).getSubgroup() == '\0' || subgroup.equals("ALL")) {
                    StudentScheduleRow studentScheduleRow = new StudentScheduleRow();
                    studentScheduleRow.setClassroom(groupSchedules.get(i).getRoom().getRoomName());
                    studentScheduleRow.setFrom(groupSchedules.get(i).getStartTime().toString());
                    studentScheduleRow.setSubject(groupSchedules.get(i).getSubjectName());
                    studentScheduleRow.setTeacher(groupSchedules.get(i).getTeacher().getTeacherName());
                    studentScheduleRow.setTo(groupSchedules.get(i).getEndTime().toString());
                    studentScheduleRow.setType(Character.toString(groupSchedules.get(i).getSubjectType()));
                    if (groupSchedules.get(i).getCalendarDay().equals("Poniedziałek"))
                        studentScheduleDays.get(0).add(studentScheduleRow);
                    if (groupSchedules.get(i).getCalendarDay().equals("Wtorek"))
                        studentScheduleDays.get(1).add(studentScheduleRow);
                    if (groupSchedules.get(i).getCalendarDay().equals("Środa"))
                        studentScheduleDays.get(2).add(studentScheduleRow);
                    if (groupSchedules.get(i).getCalendarDay().equals("Czwartek"))
                        studentScheduleDays.get(3).add(studentScheduleRow);
                    if (groupSchedules.get(i).getCalendarDay().equals("Piątek"))
                        studentScheduleDays.get(4).add(studentScheduleRow);
                    if (groupSchedules.get(i).getCalendarDay().equals("Sobota"))
                        studentScheduleDays.get(5).add(studentScheduleRow);
                    if (groupSchedules.get(i).getCalendarDay().equals("Niedziela"))
                        studentScheduleDays.get(6).add(studentScheduleRow);
                }

            }
            hideExcessButtons();
            prepareTodaySchedule();
        }
    }


    /**
     * Sets the color of button stroke.
     * @param button Button
     * @param color Color ID from Resources
     */
    private void setButtonStroke(Button button, int color) {
        GradientDrawable drawable = (GradientDrawable) button.getBackground();
        drawable.setStroke(1, ContextCompat.getColor(this, color));
    }

    /**
     * Hides buttons corresponding to days where the schedule is empty.
     */
    private void hideExcessButtons() {
        for (int dayOfWeek = 0; dayOfWeek < 7; dayOfWeek++) {
            if (studentScheduleDays.get(dayOfWeek).size() == 0) {
                buttons.get(dayOfWeek).setVisibility(View.GONE);
            }
        }
    }

    /**
     * Sets the button representing current day to active and fills the table with current day's
     * schedule.
     */
    private void prepareTodaySchedule() {
        Calendar calendar = Calendar.getInstance();
        int dayOfWeek = (calendar.get(Calendar.DAY_OF_WEEK) + 5) % 7;

        setButtonStroke(buttons.get(dayOfWeek), R.color.colorAlmostBlack);
        fillStudentScheduleTable(dayOfWeek);
    }

    /**
     * Fetches student schedule for the whole week.
     */
    private void fetchStudentScheduleDays() {
        // TODO: fetch real data once Firebase is set up
        // For now, using artificial data.
        prepareStudentScheduleDaysArrayList();
        new GetGroupSchedule().execute(scheduleURI);

    }

    /**
     * Fills the studentScheduleDays ArrayList with empty ArrayLists.
     */
    private void prepareStudentScheduleDaysArrayList() {
        studentScheduleDays = new ArrayList<>();
        for (int dayOfWeek = 0; dayOfWeek < 7; dayOfWeek++) {
            studentScheduleDays.add(new ArrayList<StudentScheduleRow>());
        }
    }

    /**
     * Adds TableRows representing student's schedule to studentScheduleTable.
     * @param dayOfWeek Day of week from which the schedule is shown.
     */
    private void fillStudentScheduleTable(int dayOfWeek) {
        ArrayList<StudentScheduleRow> studentSchedule = studentScheduleDays.get(dayOfWeek);

        int childCount;
        while ((childCount = studentScheduleTable.getChildCount()) > 1) {
            studentScheduleTable.removeViewAt(childCount - 1);
        }

        for (StudentScheduleRow studentScheduleRow : studentSchedule) {
            addStudentScheduleTableRow(studentScheduleRow);
        }
    }

    /**
     * Adds a new studentScheduleRow to studentScheduleTable.
     * @param studentScheduleRow StudentScheduleRow used to create TableRow.
     */
    private void addStudentScheduleTableRow(StudentScheduleRow studentScheduleRow) {
        TableRow tr = new TableRow(this);
        tr.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        tr.setBackgroundColor(ContextCompat.getColor(this,R.color.colorWhite));

        TextView from = createTextView(studentScheduleRow.getFrom(), 1f);
        tr.addView(from);

        TextView to = createTextView(studentScheduleRow.getTo(), 1f);
        tr.addView(to);

        TextView type = createTextView(studentScheduleRow.getType(), 1f);
        tr.addView(type);

        TextView subject = createTextView(studentScheduleRow.getSubject(), 4f);
        tr.addView(subject);

        TextView teacher = createTextView(studentScheduleRow.getTeacher(), 2f);
        tr.addView(teacher);

        TextView room = createTextView(studentScheduleRow.getClassroom(), 1f);
        tr.addView(room);
        studentScheduleTable.addView(tr,
                new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT)
        );
    }

    /**
     * Creates TextView object, using given text and weight. Used to fill student schedule.
     * @param text
     * @param weight
     * @return The created TextView.
     */
    private TextView createTextView(String text, float weight) {
        TextView tv = new TextView(this);
        tv.setText(text);
        tv.setLayoutParams(new TableRow.LayoutParams(
                0,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                weight
        ));
        tv.setBreakStrategy(Layout.BREAK_STRATEGY_SIMPLE);
        tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        return tv;
    }

    /**
     * {inheritDoc}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_schedule);

        try {
            scheduleURI = new URI(getIntent().getStringExtra("scheduleUri"));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        subgroup = getIntent().getStringExtra("subgroup");

        studentScheduleTable = findViewById(R.id.student_schedule_table);

        buttons = new ArrayList<>();

        buttons.add((Button) findViewById(R.id.button_monday));
        buttons.add((Button) findViewById(R.id.button_tuesday));
        buttons.add((Button) findViewById(R.id.button_wednesday));
        buttons.add((Button) findViewById(R.id.button_thursday));
        buttons.add((Button) findViewById(R.id.button_friday));
        buttons.add((Button) findViewById(R.id.button_saturday));
        buttons.add((Button) findViewById(R.id.button_sunday));

        fetchStudentScheduleDays();

        setupActionBar();
    }

    /**
     * Sets the clicked button border to black and fills the schedule with data from selected day of the week.
     * @param view The element that triggered this action.
     */
    public void onButtonClick(View view) {
        for (Button button : buttons) {
            setButtonStroke(button, R.color.colorGrey);
        }
        setButtonStroke((Button) view, R.color.colorAlmostBlack);

        fillStudentScheduleTable(buttons.indexOf(view));
    }
}
