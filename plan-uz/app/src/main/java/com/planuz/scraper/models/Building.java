package com.planuz.scraper.models;

import java.net.URI;

public class Building {
    private String buildingName;
    private String buildingDescription;
    private String buildingAddress;
    private URI roomsListLink;

    public Building() {
    }

    public Building(String buildingName, URI roomsListLink) {
        this.buildingName = buildingName;
        this.roomsListLink = roomsListLink;
    }

    public String toString() {
        return buildingName;
    }

    public URI getBuildingName() {
        return roomsListLink;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public URI getRoomsListLink() {
        return roomsListLink;
    }

    public void setRoomsListLink(URI roomsListLink) {
        this.roomsListLink = roomsListLink;
    }

    public String getBuildingDescription() {
        return buildingDescription;
    }

    public void setBuildingDescription(String buildingDescription) {
        this.buildingDescription = buildingDescription;
    }

    public String getBuildingAddress() {
        return buildingAddress;
    }

    public void setBuildingAddress(String buildingAddress) {
        this.buildingAddress = buildingAddress;
    }
}
