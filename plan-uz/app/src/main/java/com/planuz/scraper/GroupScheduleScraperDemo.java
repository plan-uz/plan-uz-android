package com.planuz.scraper;

import com.planuz.scraper.models.GroupSchedule;
import com.planuz.scraper.scrapers.groupsSchedule.GroupsScheduleScraper;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class GroupScheduleScraperDemo {
    public static void main(String[] args) {
        GroupsScheduleScraper groupsScheduleScraper = new GroupsScheduleScraper();
        ArrayList<GroupSchedule> groupSchedules = null;
        try {
            groupSchedules = groupsScheduleScraper.getGroupSchedule(new URI("http://www.plan.uz.zgora.pl/grupy_plan.php?pId_Obiekt=20888"));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        for (int i = 0; i< groupSchedules.size(); i++) {
            System.out.println("Item no. "+i);
            System.out.println("Week day: "+ groupSchedules.get(i).getCalendarDay());
            System.out.println("Subgroup: "+ groupSchedules.get(i).getSubgroup());
            System.out.println("Start time: "+ groupSchedules.get(i).getStartTime().toString());
            System.out.println("End time: "+ groupSchedules.get(i).getEndTime().toString());
            System.out.println("Subject name: "+ groupSchedules.get(i).getSubjectName());
            System.out.println("Subject type: "+ groupSchedules.get(i).getSubjectType());
            System.out.println("Teacher name: "+ groupSchedules.get(i).getTeacher().getTeacherName());
            System.out.println("Teacher schedule: "+ groupSchedules.get(i).getTeacher().getScheduleLink());
            System.out.println("Room name: "+ groupSchedules.get(i).getRoom().getRoomName());
            System.out.println("Room schedule: "+ groupSchedules.get(i).getRoom().getScheduleLink());
            System.out.println("Term name: "+ groupSchedules.get(i).getTerm().getTermName());
            System.out.println("Term schedule: "+ groupSchedules.get(i).getTerm().getTermLink());
            System.out.print("\n");
        }
    }
}
