package com.planuz.scraper.models;

import java.net.URI;

public class Group {
    private String groupName;
    private URI scheduleLink;

    public Group() {
    }

    public Group(String groupName, URI scheduleLink) {
        if (groupName != null) {
            this.groupName = groupName;
        }
        if (scheduleLink != null) {
            this.scheduleLink = scheduleLink;
        }
    }

    public String toString() {
        return groupName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public URI getScheduleLink() {
        return scheduleLink;
    }

    public void setScheduleLink(URI scheduleLink) {
        this.scheduleLink = scheduleLink;
    }
}
