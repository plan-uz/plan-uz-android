package com.planuz.scraper.scrapers.teachersSchedule;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.planuz.scraper.models.Group;
import com.planuz.scraper.models.Room;
import com.planuz.scraper.models.Teacher;
import com.planuz.scraper.models.TeacherSchedule;
import com.planuz.scraper.models.Term;
import com.planuz.scraper.models.TermsTable;
import com.planuz.scraper.models.Time;
import com.planuz.scraper.utils.Links;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TeachersScheduleScraper {
    private final String teacherScheduleUrlPrefix = "http://www.plan.uz.zgora.pl/nauczyciel_plan.php?pId_Obiekt=";
    private Document doc;
    private Teacher teacher;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference();

    public TeachersScheduleScraper() {
    }

    public TeachersScheduleScraper(Teacher teacher) {
        this.teacher = teacher;
    }

    public ArrayList<TeacherSchedule> getTeacherSchedule() {
        try {
            Map<String, List<String>> parameters = Links.splitQuery(teacher.getScheduleLink().toURL());
            int scheduleID = Integer.parseInt(parameters.get("pId_Obiekt").get(0));
            return getTeacherSchedule(scheduleID);
        } catch (MalformedURLException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<TeacherSchedule> getTeacherSchedule(int scheduleID) {
        try {
            doc = Jsoup.connect(teacherScheduleUrlPrefix + scheduleID).get();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Elements elements = doc.select("table tr td");
        ArrayList<TeacherSchedule> teacherSchedules = new ArrayList<>();

        for (int counter = 0; counter < elements.size(); counter++) {
            if (TermsTable.isCalendarDay(elements.get(counter).text())) {
                if (counter == elements.size() - 1) break;
                for (int i = counter + 1; TermsTable.isCalendarDay(elements.get(i).text()) == false; i++) {
                    if (i == elements.size() - 1) break;
                    if ((Time.isTime(elements.get(i).text())) && (Time.isTime(elements.get(i + 1).text())) || (elements.get(i).text().isEmpty() && elements.get(i + 1).text().isEmpty())) {
                        TeacherSchedule teacherSchedule = new TeacherSchedule();
                        teacherSchedule.setCalendarDay(elements.get(counter).text());
                        if ((elements.get(i).text().isEmpty() == false) && (elements.get(i + 1).text().isEmpty() == false)) {
                            String[] time;
                            time = elements.get(i).text().split(":");
                            teacherSchedule.setStartTime(new Time(Integer.parseInt(time[0]), Integer.parseInt(time[1])));
                            time = elements.get(i + 1).text().split(":");
                            teacherSchedule.setEndTime(new Time(Integer.parseInt(time[0]), Integer.parseInt(time[1])));
                        }
                        teacherSchedule.setSubjectName(elements.get(i + 2).text());
                        teacherSchedule.setSubjectType(elements.get(i + 3).text().charAt(0));
                        try {
                            if (elements.get(i + 4).text().isEmpty() == false)
                                teacherSchedule.setGroup(new Group(elements.get(i + 4).text(), new URI(elements.get(i + 4).selectFirst("a").attr("href"))));
                            if (elements.get(i + 5).text().isEmpty() == false) {
                                try {
                                    teacherSchedule.setRoom(new Room(elements.get(i + 5).text(), new URI(elements.get(i + 5).selectFirst("a").attr("href"))));
                                } catch (NullPointerException e) {
                                    teacherSchedule.setRoom(new Room(elements.get(i + 5).text(), null));
                                }

                            }
                            teacherSchedule.setTerm(new Term(elements.get(i + 6).text(), new URI(elements.get(i + 6).selectFirst("a").attr("href"))));
                        } catch (URISyntaxException e) {
                            e.printStackTrace();
                        }
                        myRef.child("teacherSchedules").child("teacherSchedule" + counter).setValue(teacherSchedule);
                        teacherSchedules.add(teacherSchedule);
                    }
                }
            }
        }

        return teacherSchedules;
    }
}
