package com.planuz.scraper.scrapers.buildings;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.planuz.scraper.models.Building;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class BuildingsScraper {
    private final String allRoomsUrl = "http://www.plan.uz.zgora.pl/sale_lista_budynkow.php?pTyp=P";
    private final String roomsUrlPrefix = "http://www.plan.uz.zgora.pl/";
    private Document doc;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference();

    public BuildingsScraper() {
        {
            try {
                doc = Jsoup.connect(allRoomsUrl).get();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public ArrayList<Building> getBuildings() {
        Elements elements = doc.select("tr:not(.gray)");
        ArrayList<Building> buildings = new ArrayList();
        for (int counter = 0; counter < elements.size(); counter++) {
            Building building = new Building();
            building.setBuildingName(elements.get(counter).select("td a").first().text());
            building.setBuildingDescription(elements.get(counter).select("td").get(1).text());
            building.setBuildingAddress(elements.get(counter).select("td").get(2).text());
            try {
                building.setRoomsListLink(new URI(
                        roomsUrlPrefix + elements.get(counter).select("td a").first().attr("href"))
                );
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            myRef.child("buildings").child("buildings" + counter).setValue(building);
            buildings.add(building);
        }
        return buildings;
    }
}
