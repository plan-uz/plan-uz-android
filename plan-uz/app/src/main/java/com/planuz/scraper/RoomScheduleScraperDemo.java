package com.planuz.scraper;

import com.planuz.scraper.models.Room;
import com.planuz.scraper.models.RoomSchedule;
import com.planuz.scraper.scrapers.buildings.BuildingsScraper;
import com.planuz.scraper.scrapers.rooms.RoomsScraper;
import com.planuz.scraper.scrapers.roomsSchedule.RoomsScheduleScraper;

import java.util.ArrayList;

public class RoomScheduleScraperDemo {
    public static void main(String[] args) {
        BuildingsScraper buildingsScraper = new BuildingsScraper();
        RoomsScraper roomsScraper = new RoomsScraper(buildingsScraper.getBuildings().get(0).getRoomsListLink());
        ArrayList<Room> rooms = roomsScraper.getRooms();
        RoomsScheduleScraper roomsScheduleScraper = new RoomsScheduleScraper();
        ArrayList<RoomSchedule> roomSchedules = roomsScheduleScraper.getRoomSchedule(rooms.get(0).getScheduleID());
        for (int i = 0; i < roomSchedules.size(); i++) {
            System.out.println("Item no. " + (i + 1));
            System.out.println("Week day: " + roomSchedules.get(i).getCalendarDay());
            System.out.println("Start time: " + roomSchedules.get(i).getStartTime());
            System.out.println("End time: " + roomSchedules.get(i).getEndTime());
            System.out.println("Subject name: " + roomSchedules.get(i).getSubjectName());
            System.out.println("Subject type: " + roomSchedules.get(i).getSubjectType());
            System.out.println("Group: " + roomSchedules.get(i).getGroup());
            System.out.println("Teacher: " + roomSchedules.get(i).getTeacher());
            System.out.println("Term: " + roomSchedules.get(i).getTerm());
            System.out.print("\n");
        }
    }
}
