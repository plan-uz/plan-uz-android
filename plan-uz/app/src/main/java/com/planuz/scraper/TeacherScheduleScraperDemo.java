package com.planuz.scraper;

import com.planuz.scraper.models.Teacher;
import com.planuz.scraper.models.TeacherSchedule;
import com.planuz.scraper.scrapers.teachersSchedule.TeachersScheduleScraper;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class TeacherScheduleScraperDemo {
    public static void main(String[] args) {
        try {
            Teacher teacher = new Teacher();
            teacher.setTeacherName("dr inż. Marcel Luzar");
            teacher.setScheduleLink(new URI("http://www.plan.uz.zgora.pl/nauczyciel_plan.php?pId_Obiekt=34712&pTyp=P"));
            TeachersScheduleScraper teachersScheduleScraper = new TeachersScheduleScraper(teacher);
            ArrayList<TeacherSchedule> teacherSchedules = teachersScheduleScraper.getTeacherSchedule();
            for (int i = 0; i < teacherSchedules.size(); i++) {
                System.out.println("Item no. " + (i + 1));
                System.out.println("Week day: " + teacherSchedules.get(i).getCalendarDay());
                System.out.println("Start time: " + teacherSchedules.get(i).getStartTime());
                System.out.println("End time: " + teacherSchedules.get(i).getEndTime());
                System.out.println("Subject name: " + teacherSchedules.get(i).getSubjectName());
                System.out.println("Subject type: " + teacherSchedules.get(i).getSubjectType());
                System.out.println("Group: " + teacherSchedules.get(i).getGroup());
                System.out.println("Room: " + teacherSchedules.get(i).getRoom());
                System.out.println("Term: " + teacherSchedules.get(i).getTerm());
                System.out.print("\n");
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
