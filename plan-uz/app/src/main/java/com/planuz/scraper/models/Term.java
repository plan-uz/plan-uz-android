package com.planuz.scraper.models;

import java.net.URI;

public class Term {
    private String termName;
    private URI termLink;

    public String toString() {
        return termName;
    }

    public String getTermName() {
        return termName;
    }

    public void setTermName(String termName) {
        this.termName = termName;
    }

    public URI getTermLink() {
        return termLink;
    }

    public void setTermLink(URI termLink) {
        this.termLink = termLink;
    }

    public Term() {}

    public Term(String termName, URI termLink) {
        this.termName = termName;
        this.termLink = termLink;
    }
}
