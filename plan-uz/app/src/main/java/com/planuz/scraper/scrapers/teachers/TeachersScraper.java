package com.planuz.scraper.scrapers.teachers;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.planuz.scraper.models.Teacher;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class TeachersScraper {
    private final String allTeachersUrl = "http://www.plan.uz.zgora.pl/nauczyciel_lista_wydzialu.php?pId_jedn=34&pTyp=P";
    private final String teacherUrlPrefix = "http://www.plan.uz.zgora.pl/";

    private Document doc;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference();

    public TeachersScraper() {
        {
            try {
                doc = Jsoup.connect(allTeachersUrl).get();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public ArrayList<Teacher> getTeachers() {
        Elements elements = doc.select("tr:not(.gray)");
        ArrayList<Teacher> teachers = new ArrayList();
        for (int counter = 0; counter < elements.size(); counter++) {
            Teacher teacher = new Teacher();
            teacher.setUnitName(elements.get(counter).select("td").get(1).text());
            teacher.setTeacherName(elements.get(counter).select("td").first().text());
            try {
                teacher.setScheduleLink(new URI(
                        teacherUrlPrefix + elements.get(counter).select("td a").first().attr("href"))
                );
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            myRef.child("teachers").child("teacher" + counter).setValue(teacher);
            teachers.add(teacher);
        }
        return teachers;
    }
}
