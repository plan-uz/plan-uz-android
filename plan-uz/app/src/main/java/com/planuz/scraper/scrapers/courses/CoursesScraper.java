package com.planuz.scraper.scrapers.courses;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.planuz.R;
import com.planuz.scraper.models.Term;

import org.angmarch.views.NiceSpinner;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class CoursesScraper extends AsyncTask<Context, Void, Document> {
    private final String allCoursesUrl = "http://www.plan.uz.zgora.pl/grupy_lista_kierunkow.php";
    private final String courseUrlPrefix = "http://www.plan.uz.zgora.pl/";
    private Document doc;
    private Context context;
    NiceSpinner courseSpinner;
    ArrayAdapter<String> adapter;
    ArrayList<Term> courses = new ArrayList();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference();

    public void setCourseSpinner(NiceSpinner courseSpinner) {
        this.courseSpinner = courseSpinner;
    }

    @Override
    protected Document doInBackground(Context... contexts) {
        context = contexts[0];
        try {
            this.doc = Jsoup.connect(allCoursesUrl).get();
            this.setAdapter();
        } catch (IOException e) {
            Log.e("connection error", e.getMessage());
            e.printStackTrace();
        }
        return doc;
    }

    public CoursesScraper() {
    }

    private void setAdapter() {
        this.courses = this.getCourses();
        List<String> coursesNames = new ArrayList<>();
        coursesNames.add("Select course");

        for (int i = 0; i < courses.size(); i++) {
            coursesNames.add(courses.get(i).getTermName());
        }
        adapter = new ArrayAdapter<String>(context, R.layout.spinner_item, new ArrayList<>(coursesNames)) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }
        };
        courseSpinner.setAdapter(adapter);
    }

    public ArrayList<Term> getCourses() {
        Elements elements = doc.select(".container.main > .list-group > .list-group-item");
        elements = elements.get(6).select(".list-group-item > .list-group > .list-group-item");
        for (int counter = 0; counter < elements.size(); counter++) {
            Term term = new Term();
            term.setTermName(elements.get(counter).text().replaceAll("\\s\\(.+$", ""));
            try {
                term.setTermLink(new URI(
                        courseUrlPrefix + elements.get(counter).select("a").attr("href"))
                );
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            myRef.child("courses").child("course" + counter).setValue(term);
            courses.add(term);
        }
        return courses;
    }

    public URI getCourseUriByCourseIndex(int index) throws URISyntaxException {
        URI uriToReturn = new URI("Not_found");
        if (this.courses.size() > 1 && this.courses.get(index) != null) {
            uriToReturn = this.courses.get(index).getTermLink();
        }
        return uriToReturn;
    }
}
