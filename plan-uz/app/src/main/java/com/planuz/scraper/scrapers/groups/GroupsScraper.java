package com.planuz.scraper.scrapers.groups;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.planuz.R;
import com.planuz.scraper.models.Term;

import org.angmarch.views.NiceSpinner;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class GroupsScraper extends AsyncTask<Context, Void, Document> {
    private final String groupUrlPrefix = "http://www.plan.uz.zgora.pl/";
    private Document doc;
    private Context context;
    private URI courseUri;
    NiceSpinner groupSpinner;
    ArrayAdapter<String> adapter;
    ArrayList<Term> groups = new ArrayList();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference();

    public GroupsScraper(URI courseUri) {
        this.courseUri = courseUri;
    }

    public void setGroupSpinner(NiceSpinner groupSpinner) {
        this.groupSpinner = groupSpinner;
    }

    @Override
    protected Document doInBackground(Context... contexts) {
        context = contexts[0];
        try {
            doc = Jsoup.connect(courseUri.toString()).get();
            this.setAdapter();
        } catch (IOException e) {
            Log.e("connection error", e.getMessage());
            e.printStackTrace();
        }
        return doc;
    }

    public ArrayList<Term> getGroups() {
        Elements elements = doc.select("table tr td");
        String groupIndex = (courseUri.toString()).substring((courseUri.toString()).length() - 4);
        for (int counter = 0; counter < elements.size(); counter++) {
            Term term = new Term();
            term.setTermName(elements.get(counter).text().replaceAll("\\s\\(.+$", ""));
            try {
                term.setTermLink(new URI(
                        groupUrlPrefix + elements.get(counter).select("a").attr("href"))
                );
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            myRef.child("groups").child(groupIndex).child("group" + counter).setValue(term);
            groups.add(term);
        }
        return groups;
    }

    private void setAdapter() {
        ArrayList<Term> groups = this.getGroups();
        List<String> groupsNames = new ArrayList<>();
        groupsNames.add("Select group");

        for (int i = 0; i < groups.size(); i++) {
            groupsNames.add(groups.get(i).getTermName());
        }
        adapter = new ArrayAdapter<String>(context, R.layout.spinner_item, new ArrayList<>(groupsNames)) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }
        };
        groupSpinner.setAdapter(adapter);
    }

    public URI getGroupUriByGroupIndex(int index) throws URISyntaxException {
        URI uriToReturn = new URI("Not_found");
        if (this.groups.size() > 1 && this.groups.get(index) != null) {
            uriToReturn = this.groups.get(index).getTermLink();
        }
        return uriToReturn;
    }

}
