package com.planuz.scraper.scrapers.roomsSchedule;


import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.planuz.scraper.models.Group;
import com.planuz.scraper.models.Room;
import com.planuz.scraper.models.RoomSchedule;
import com.planuz.scraper.models.Teacher;
import com.planuz.scraper.models.Term;
import com.planuz.scraper.models.TermsTable;
import com.planuz.scraper.models.Time;
import com.planuz.scraper.utils.Links;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RoomsScheduleScraper {
    private final String roomScheduleUrlPrefix = "http://www.plan.uz.zgora.pl/sale_plan.php?pId_Obiekt=";
    private Document doc;
    private Room room;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference();

    public RoomsScheduleScraper() {
    }

    public RoomsScheduleScraper(Room room) {
        this.room = room;
    }

    public ArrayList<RoomSchedule> getRoomSchedule() {
        try {
            Map<String, List<String>> parameters = Links.splitQuery(room.getScheduleLink().toURL());
            int scheduleID = Integer.parseInt(parameters.get("pId_Obiekt").get(0));
            return getRoomSchedule(scheduleID);
        } catch (MalformedURLException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<RoomSchedule> getRoomSchedule(int scheduleID) {
        try {
            doc = Jsoup.connect(roomScheduleUrlPrefix + scheduleID).get();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Elements elements = doc.select("table tr td");
        ArrayList<RoomSchedule> roomSchedules = new ArrayList<>();

        for (int counter = 0; counter < elements.size(); counter++) {
            if (TermsTable.isCalendarDay(elements.get(counter).text())) {
                if (counter == elements.size() - 1) break;
                for (int i = counter + 1; TermsTable.isCalendarDay(elements.get(i).text()) == false; i++) {
                    if (i == elements.size() - 1) break;
                    if ((Time.isTime(elements.get(i).text())) && (Time.isTime(elements.get(i + 1).text()))) {
                        RoomSchedule roomSchedule = new RoomSchedule();
                        String[] time;
                        roomSchedule.setCalendarDay(elements.get(counter).text());
                        time = elements.get(i).text().split(":");
                        roomSchedule.setStartTime(new Time(Integer.parseInt(time[0]), Integer.parseInt(time[1])));
                        time = elements.get(i + 1).text().split(":");
                        roomSchedule.setEndTime(new Time(Integer.parseInt(time[0]), Integer.parseInt(time[1])));
                        roomSchedule.setSubjectName(elements.get(i + 2).text());
                        roomSchedule.setSubjectType(elements.get(i + 3).text().charAt(0));
                        try {
                            roomSchedule.setGroup(
                                    new Group(elements.get(i + 4).text(),
                                            new URI(elements.get(i + 4).selectFirst("a") != null
                                                    ? elements.get(i + 4).selectFirst("a").attr("href")
                                                    : "")
                                    ));
                            roomSchedule.setTeacher(new Teacher(elements.get(i + 5).text(), new URI(elements.get(i + 5).selectFirst("a").attr("href"))));
                            roomSchedule.setTerm(new Term(elements.get(i + 6).text(), new URI(elements.get(i + 6).selectFirst("a").attr("href"))));
                        } catch (URISyntaxException e) {
                            e.printStackTrace();
                        }
                        myRef.child("roomSchedule").child("roomSchedule" + counter).setValue(roomSchedule);
                        roomSchedules.add(roomSchedule);
                    }
                }
            }
        }

        return roomSchedules;
    }
}
