package com.planuz.scraper.scrapers.groupsSchedule;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.planuz.scraper.models.Group;
import com.planuz.scraper.models.GroupSchedule;
import com.planuz.scraper.models.Room;
import com.planuz.scraper.models.Teacher;
import com.planuz.scraper.models.Term;
import com.planuz.scraper.models.TermsTable;
import com.planuz.scraper.models.Time;
import com.planuz.scraper.utils.Links;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GroupsScheduleScraper {
    private final String groupScheduleUrlPrefix = "http://www.plan.uz.zgora.pl/grupy_plan.php?pId_Obiekt=";
    private Document doc;
    private Group group;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference();

    public GroupsScheduleScraper() {
    }

    public GroupsScheduleScraper(Group group) {
        this.group = group;
    }

    public ArrayList<GroupSchedule> getGroupSchedule() {
        try {
            Map<String, List<String>> parameters = Links.splitQuery(group.getScheduleLink().toURL());
            int scheduleID = Integer.parseInt(parameters.get("pId_Obiekt").get(0));
            return getGroupSchedule(new URI(groupScheduleUrlPrefix+Integer.toString(scheduleID)));
        } catch (MalformedURLException | UnsupportedEncodingException | URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<GroupSchedule> getGroupSchedule(URI uri) {
        try {
            doc = Jsoup.connect(uri.toString()).get();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Elements elements = doc.select("table tr td");
        ArrayList<GroupSchedule> groupSchedules = new ArrayList<>();
        String groupIndex = (uri.toString()).substring((uri.toString()).length() - 4);
        for (int counter = 0; counter < elements.size(); counter++) {
            if (TermsTable.isIrregular(elements.get(counter).text())) break;
            if (TermsTable.isCalendarDay(elements.get(counter).text())) {
                if (counter == elements.size() - 1) break;
                for (int i = counter + 1; TermsTable.isCalendarDay(elements.get(i).text()) == false; i++) {
                    if (i == elements.size() - 1) break;
                    if ((elements.get(i).text().length() == 1) || (elements.get(i).text().length() == 0)) {
                        if (elements.get(i).text().length() == 0 || GroupSchedule.isSubgroup(elements.get(i).text().charAt(0))) {
                            GroupSchedule groupSchedule = new GroupSchedule();
                            String[] time;
                            groupSchedule.setCalendarDay(elements.get(counter).text());
                            if (elements.get(i).text().length() == 1)
                                groupSchedule.setSubgroup(elements.get(i).text().charAt(0));
                            if (elements.get(i+1).text().isEmpty() == false) {
                                time = elements.get(i + 1).text().split(":");
                                groupSchedule.setStartTime(new Time(Integer.parseInt(time[0]), Integer.parseInt(time[1])));
                            }
                            if (elements.get(i+2).text().isEmpty() == false) {
                                time = elements.get(i + 2).text().split(":");
                                groupSchedule.setEndTime(new Time(Integer.parseInt(time[0]), Integer.parseInt(time[1])));
                            }
                            groupSchedule.setSubjectName(elements.get(i + 3).text());
                            groupSchedule.setSubjectType(elements.get(i + 4).text().charAt(0));
                            try {
                                if (elements.get(i + 5).text().isEmpty() == false) {
                                    try {
                                        groupSchedule.setTeacher(new Teacher(elements.get(i + 5).text(), new URI(elements.get(i + 5).selectFirst("a").attr("href"))));
                                    } catch (NullPointerException e) {
                                        groupSchedule.setTeacher(new Teacher(elements.get(i + 5).text(), null));
                                    }
                                }
                                if (elements.get(i + 6).text().isEmpty() == false)  {
                                    try {
                                        groupSchedule.setRoom(new Room(elements.get(i + 6).text(), new URI(elements.get(i + 6).selectFirst("a").attr("href"))));
                                    } catch (NullPointerException e) {
                                        groupSchedule.setRoom(new Room(elements.get(i + 6).text(), null));
                                    }
                                }
                                if (elements.get(i + 7).text().isEmpty() == false)  {
                                    try {
                                        groupSchedule.setTerm(new Term(elements.get(i + 7).text(), new URI(elements.get(i + 7).selectFirst("a").attr("href"))));
                                    } catch (NullPointerException e) {
                                        groupSchedule.setTerm(new Term(elements.get(i + 7).text(), null));
                                    }
                                }

                            } catch (URISyntaxException e) {
                                e.printStackTrace();
                            }
                            myRef.child("groupSchedule").child(groupIndex).child("groupSchedule" + counter).setValue(groupSchedule.toString());
                            groupSchedules.add(groupSchedule);
                        }
                    }
                }
            }
        }

        return groupSchedules;
    }
}
