package com.planuz.scraper.models;

import java.util.Calendar;
import java.util.Date;

public class Time {
    private int hours, minutes;

    public Time() {

    }

    public Time(int hours, int minutes) {
        this.hours = hours;
        this.minutes = minutes;
    }

    public static boolean isTime(String timeString) {
        return timeString.matches("^(0[0-9]|1[0-9]|2[0-3]|[0-9]):[0-5][0-9]$");
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        if (hours >= 0 && hours < 24) {
            this.hours = hours;
        }
        else {
            throw new NumberFormatException("Hours must be between 0 and 23!");
        }
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        if (minutes >= 0 && minutes < 60) {
            this.minutes = minutes;
        } else {
            throw new NumberFormatException("Minutes must be between 0 and 59!");
        }
    }

    public String toString() {
        String hours, minutes;
        if (this.hours < 10) {
            hours = "0" + this.hours;
        } else {
            hours = Integer.toString(this.hours);
        }

        if (this.minutes < 10) {
            minutes = "0" + this.minutes;
        } else {
            minutes = Integer.toString(this.minutes);
        }
        return hours+":"+minutes;
    }

    public Date toDate(Date date) {
        Calendar calendar = Calendar.getInstance();

        calendar.setTime(date);
        calendar.set(Calendar.HOUR, hours);
        calendar.set(Calendar.MINUTE, minutes);

        return calendar.getTime();
    }
}
