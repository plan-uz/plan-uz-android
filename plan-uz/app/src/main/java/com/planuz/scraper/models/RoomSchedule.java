package com.planuz.scraper.models;

public class RoomSchedule {
    private Time startTime, endTime;
    private String subjectName;
    private char subjectType;
    private Group group;
    private Teacher teacher;
    private Term term;
    private String calendarDay;

    public RoomSchedule() {}

    public RoomSchedule(Time startTime, Time endTime, String subjectName, char subjectType, Group group, Teacher teacher, Term term, String calendarDay) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.subjectName = subjectName;
        this.subjectType = subjectType;
        this.group = group;
        this.teacher = teacher;
        this.term = term;
        this.calendarDay = calendarDay;
    }

    public String getCalendarDay() {
        return calendarDay;
    }

    public void setCalendarDay(String calendarDay) {
        this.calendarDay = calendarDay;
    }

    public Time getStartTime() {
        return startTime;
    }

    public void setStartTime(Time startTime) {
        this.startTime = startTime;
    }

    public Time getEndTime() {
        return endTime;
    }

    public void setEndTime(Time endTime) {
        this.endTime = endTime;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public char getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(char subjectType) {
        this.subjectType = subjectType;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Term getTerm() {
        return term;
    }

    public void setTerm(Term term) {
        this.term = term;
    }
}
