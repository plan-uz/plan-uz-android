package com.planuz;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;


/**
 * Activity class that contains functions needed to create a navbar. All activities in this project
 * extend this class.
 */
public class AppActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;

    private Context context;

    /**
     * Sets up the action bar in the top of the screen.
     */
    public void setupActionBar() {
        drawerLayout = findViewById(R.id.drawer_layout);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);

        final Context localContext = context;

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch(menuItem.getItemId()) {
                    case R.id.nav_notifications: {
                        break;
                    }
                    case R.id.nav_teachers_schedule: {
                        Intent intent = new Intent(localContext, TeacherMainMenuActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case R.id.nav_rooms_schedule: {
                        Intent intent = new Intent(localContext, RoomsMainMenuActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case R.id.nav_starred_groups: {
                        break;
                    }
                }
                drawerLayout.closeDrawers();
                return true;
            }
        });
    }

    /**
     * {inheritDoc}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
    }

    /**
     * {inheritDoc}
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
