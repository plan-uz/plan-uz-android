package com.planuz;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.planuz.scraper.models.Building;
import com.planuz.scraper.models.Room;
import com.planuz.scraper.scrapers.buildings.BuildingsScraper;
import com.planuz.scraper.scrapers.rooms.RoomsScraper;

import org.angmarch.views.NiceSpinner;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class RoomsMainMenuActivity extends AppActivity {

    private ListView listView;
    private Context context;
    private ArrayList<Room> rooms;
    private ArrayList<Building> buildings;
    private NiceSpinner niceSpinner;

    /**
     * Async task for getting room list
     */
    private class GetRoomList extends AsyncTask<URI, Void, Void> {
        @Override
        protected Void doInBackground(URI... uris) {
            RoomsScraper roomsScraper = new RoomsScraper(uris[0]);
            rooms = roomsScraper.getRooms();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            RoomsAdapter roomsAdapter = new RoomsAdapter(context, rooms);
            listView.setAdapter(roomsAdapter);
        }
    }

    private class GetBuildingsList extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            BuildingsScraper buildingsScraper = new BuildingsScraper();
            buildings = buildingsScraper.getBuildings();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            BuildingsAdapter buildingsAdapter = new BuildingsAdapter(context, buildings);
            niceSpinner.setAdapter(buildingsAdapter);
            fillListView(buildings.get(0).getRoomsListLink());
        }
    }

    private void fillSpinner() {
        new GetBuildingsList().execute();
    }

    /**
     * Fills list view with room list
     */
    private void fillListView(URI buildingUri) {
        new GetRoomList().execute(buildingUri);
    }

    /**
     * {inheritDoc}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rooms_main_menu);
        listView = findViewById(R.id.roomsListView);
        niceSpinner = findViewById(R.id.buildingsSpinner);
        context = this;
        fillSpinner();

        niceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                fillListView(buildings.get(position).getRoomsListLink());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
