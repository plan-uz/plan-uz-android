package com.planuz;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import org.angmarch.views.NiceSpinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The configuration screen for the {@link StudentScheduleWidget StudentScheduleWidget} AppWidget.
 */
public class StudentScheduleWidgetConfigureActivity extends Activity {

    private static final String PREFS_NAME = "com.planuz.StudentScheduleWidget";
    private static final String PREF_PREFIX_KEY = "appwidget_";
    int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;

    NiceSpinner spinnerCourse;
    NiceSpinner spinnerGroup;
    NiceSpinner spinnerSubgroup;
    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            final Context context = StudentScheduleWidgetConfigureActivity.this;

            // When the button is clicked, store the string locally

            String courseText = spinnerCourse.getText().toString(); /** Extracts selection from spinners */
            String groupText = spinnerGroup.getText().toString();
            String subgroupText = spinnerSubgroup.getText().toString();

            saveSelectionPref(context, mAppWidgetId, courseText, groupText, subgroupText); /** Saves selection */

            // It is the responsibility of the configuration activity to update the app widget
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            StudentScheduleWidget.updateAppWidget(context, appWidgetManager, mAppWidgetId);

            // Make sure we pass back the original appWidgetId
            Intent resultValue = new Intent();
            resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
            setResult(RESULT_OK, resultValue);
            finish();
        }
    };

    public StudentScheduleWidgetConfigureActivity() {
        super();
    }



    /**
     * Saves selected preferences to SharedPreferences object for this widget
     */
    static void saveSelectionPref(Context context, int appWidgetId, String course, String group, String subgroup) {
        SharedPreferences.Editor prefs = context.getSharedPreferences(PREFS_NAME, 0).edit();
        prefs.putString(PREF_PREFIX_KEY + "course_" + appWidgetId, course);
        prefs.putString(PREF_PREFIX_KEY + "group_" + appWidgetId, group);
        prefs.putString(PREF_PREFIX_KEY + "subgroup_" + appWidgetId, subgroup);
        prefs.apply();
    }

    /**
     * Reads and returns saved course selection from SharedPreferences object
     */
    static String loadCoursePref(Context context, int appWidgetId) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        String prefValue = prefs.getString(PREF_PREFIX_KEY + "course_" + appWidgetId, null);
        if (prefValue != null) {
            return prefValue;
        } else {
            return "error";
        }
    }

    /**
     * Reads and returns saved group selection from SharedPreferences object
     */
    static String loadGroupPref(Context context, int appWidgetId) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        String prefValue = prefs.getString(PREF_PREFIX_KEY + "group_" + appWidgetId, null);
        if (prefValue != null) {
            return prefValue;
        } else {
            return "error";
        }
    }

    /**
     * Reads and returns saved subgroup selection from SharedPreferences object
     */
    static String loadSubgroupPref(Context context, int appWidgetId) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        String prefValue = prefs.getString(PREF_PREFIX_KEY + "subgroup_" + appWidgetId, null);
        if (prefValue != null) {
            return prefValue;
        } else {
            return "error";
        }
    }

    /**
     * Deletes saved selection preferences from SharedPreferences object
     */
    static void deleteSelectionPref(Context context, int appWidgetId) {
        SharedPreferences.Editor prefs = context.getSharedPreferences(PREFS_NAME, 0).edit();
        prefs.remove(PREF_PREFIX_KEY + "course_" + appWidgetId);
        prefs.remove(PREF_PREFIX_KEY + "group_" + appWidgetId);
        prefs.remove(PREF_PREFIX_KEY + "subgroup_" + appWidgetId);
        prefs.apply();
    }



    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        // Set the result to CANCELED.  This will cause the widget host to cancel
        // out of the widget placement if the user presses the back button.
        setResult(RESULT_CANCELED);

        setContentView(R.layout.student_schedule_widget_configure);

        spinnerCourse = (NiceSpinner) findViewById(R.id.courseSpinnerConfigWS); /** Finds spinners */
        spinnerGroup = (NiceSpinner) findViewById(R.id.groupSpinnerConfigWS);
        spinnerSubgroup = (NiceSpinner) findViewById(R.id.subgroupSpinnerConfigWS);

        findViewById(R.id.add_button).setOnClickListener(mOnClickListener);
        fillSpinners();

        // Find the widget id from the intent.
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mAppWidgetId = extras.getInt(
                    AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        // If this activity was started with an intent without an app widget ID, finish with an error.
        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
            return;
        }


    }


    /**
     * Fills the spinners with data.
     */
    private void fillSpinners() {
        NiceSpinner courseSpinner = findViewById(R.id.courseSpinnerConfigWS);
        NiceSpinner groupSpinner = findViewById(R.id.groupSpinnerConfigWS);
        NiceSpinner subgroupSpinner = findViewById(R.id.subgroupSpinnerConfigWS);

        String[] courses = new String[] {
                "Select course name",
                "Option 1",
                "Option 2",
                "Option 3"
        };
        List<String> coursesList = new ArrayList<>(Arrays.asList(courses));

        String[] groups = new String[] {
                "Select group name",
                "Option 1",
                "Option 2",
                "Option 3"
        };
        List<String> groupsList = new ArrayList<>(Arrays.asList(groups));

        String[] subgroups = new String[] {
                "Select subgroup",
                "A",
                "B"
        };
        List<String> subgroupsList = new ArrayList<>(Arrays.asList(subgroups));

        ArrayAdapter<String> courseSpinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, coursesList) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }
        };

        courseSpinner.setAdapter(courseSpinnerArrayAdapter);

        ArrayAdapter<String> groupSpinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, groupsList) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }
        };

        groupSpinner.setAdapter(groupSpinnerArrayAdapter);

        ArrayAdapter<String> subgroupSpinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, subgroupsList) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }
        };

        subgroupSpinner.setAdapter(subgroupSpinnerArrayAdapter);
    }

}

