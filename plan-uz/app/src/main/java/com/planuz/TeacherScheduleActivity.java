package com.planuz;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Layout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;

public class TeacherScheduleActivity extends AppActivity {

    private ArrayList<Button> buttons;
    private ArrayList<ArrayList<TeacherScheduleRow>> teacherScheduleDays;
    private TableLayout teacherScheduleTable;

    /**
     * Sets the color of button stroke.
     * @param button Button
     * @param color Color ID from Resources
     */
    private void setButtonStroke(Button button, int color) {
        GradientDrawable drawable = (GradientDrawable) button.getBackground();
        drawable.setStroke(1, ContextCompat.getColor(this, color));
    }

    /**
     * Hides buttons corresponding to days where the schedule is empty.
     */
    private void hideExcessButtons() {
        for (int dayOfWeek = 0; dayOfWeek < 7; dayOfWeek++) {
            if (teacherScheduleDays.get(dayOfWeek).size() == 0) {
                buttons.get(dayOfWeek).setVisibility(View.GONE);
            }
        }
    }

    /**
     * Sets the button representing current day to active and fills the table with current day's
     * schedule.
     */
    private void prepareTodaySchedule() {
        Calendar calendar = Calendar.getInstance();
        int dayOfWeek = (calendar.get(Calendar.DAY_OF_WEEK) + 5) % 7;

        setButtonStroke(buttons.get(dayOfWeek), R.color.colorAlmostBlack);
        fillTeacherScheduleTable(dayOfWeek);
    }

    /**
     * Fetches teacher schedule for the whole week.
     */
    private void fetchTeacherScheduleDays() {
        // TODO: fetch real data once Firebase is set up
        // For now, using artificial data.
        prepareTeacherScheduleDaysArrayList();

        TeacherScheduleRow ssr1 = new TeacherScheduleRow();
        ssr1.setFrom("7:30");
        ssr1.setTo("9:00");
        ssr1.setSubject("Zarządzanie przemysłowym projektem informatycznym");
        ssr1.setType("L");
        ssr1.setGroup("33INF-SP/A");
        ssr1.setClassroom("319 A-2");

        teacherScheduleDays.get(0).add(ssr1);
        teacherScheduleDays.get(0).add(ssr1);

        teacherScheduleDays.get(1).add(ssr1);

        teacherScheduleDays.get(3).add(ssr1);
        teacherScheduleDays.get(4).add(ssr1);
        teacherScheduleDays.get(4).add(ssr1);
        teacherScheduleDays.get(4).add(ssr1);

        teacherScheduleDays.get(6).add(ssr1);
    }

    /**
     * Fills the teacherScheduleDays ArrayList with empty ArrayLists.
     */
    private void prepareTeacherScheduleDaysArrayList() {
        teacherScheduleDays = new ArrayList<>();
        for (int dayOfWeek = 0; dayOfWeek < 7; dayOfWeek++) {
            teacherScheduleDays.add(new ArrayList<TeacherScheduleRow>());
        }
    }

    /**
     * Adds TableRows representing teacher's schedule to teacherScheduleTable.
     * @param dayOfWeek Day of week from which the schedule is shown.
     */
    private void fillTeacherScheduleTable(int dayOfWeek) {
        ArrayList<TeacherScheduleRow> teacherSchedule = teacherScheduleDays.get(dayOfWeek);

        int childCount;
        while ((childCount = teacherScheduleTable.getChildCount()) > 1) {
            teacherScheduleTable.removeViewAt(childCount - 1);
        }

        for (TeacherScheduleRow teacherScheduleRow : teacherSchedule) {
            addTeacherScheduleTableRow(teacherScheduleRow);
        }
    }

    /**
     * Adds a new studentScheduleRow to teacherScheduleTable.
     * @param teacherScheduleRow StudentScheduleRow used to create TableRow.
     */
    private void addTeacherScheduleTableRow(TeacherScheduleRow teacherScheduleRow) {
        TableRow tr = new TableRow(this);
        tr.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        tr.setBackgroundColor(ContextCompat.getColor(this,R.color.colorWhite));

        TextView from = createTextView(teacherScheduleRow.getFrom(), 1f);
        tr.addView(from);

        TextView to = createTextView(teacherScheduleRow.getTo(), 1f);
        tr.addView(to);

        TextView type = createTextView(teacherScheduleRow.getType(), 1f);
        tr.addView(type);

        TextView subject = createTextView(teacherScheduleRow.getSubject(), 4f);
        tr.addView(subject);

        TextView teacher = createTextView(teacherScheduleRow.getGroup(), 2f);
        tr.addView(teacher);

        TextView room = createTextView(teacherScheduleRow.getClassroom(), 1f);
        tr.addView(room);
        teacherScheduleTable.addView(tr,
                new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT)
        );
    }

    /**
     * Creates the TextView used in TableRow, sets text and weight.
     * @param text The displayed text
     * @param weight The weight of the TextView
     * @return
     */
    private TextView createTextView(String text, float weight) {
        TextView tv = new TextView(this);
        tv.setText(text);
        tv.setLayoutParams(new TableRow.LayoutParams(
                0,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                weight
        ));
        tv.setBreakStrategy(Layout.BREAK_STRATEGY_SIMPLE);
        tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        return tv;
    }

    /**
     * {inheritDoc}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_schedule);

        teacherScheduleTable = findViewById(R.id.student_schedule_table);

        buttons = new ArrayList<>();

        buttons.add((Button) findViewById(R.id.button_monday));
        buttons.add((Button) findViewById(R.id.button_tuesday));
        buttons.add((Button) findViewById(R.id.button_wednesday));
        buttons.add((Button) findViewById(R.id.button_thursday));
        buttons.add((Button) findViewById(R.id.button_friday));
        buttons.add((Button) findViewById(R.id.button_saturday));
        buttons.add((Button) findViewById(R.id.button_sunday));

        fetchTeacherScheduleDays();

        hideExcessButtons();
        prepareTodaySchedule();

        setupActionBar();
    }

    /**
     * Sets the clicked button border to black and fills the schedule with data from selected day of the week.
     * @param view The element that triggered this action.
     */
    public void onButtonClick(View view) {
        for (Button button : buttons) {
            setButtonStroke(button, R.color.colorGrey);
        }
        setButtonStroke((Button) view, R.color.colorAlmostBlack);

        fillTeacherScheduleTable(buttons.indexOf(view));
    }
}
