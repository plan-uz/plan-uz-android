package com.planuz;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.planuz.scraper.models.Building;
import com.planuz.scraper.models.Room;

import java.util.ArrayList;

public class BuildingsAdapter extends ArrayAdapter<Building> {
    public BuildingsAdapter(Context context, ArrayList<Building> buildings) {
        super(context, 0, buildings);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Building building = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.spinner_item, parent, false);
        }

        TextView textView = convertView.findViewById(R.id.spinnerTextView);
        textView.setText(building.getBuildingDescription());

        return convertView;
    }
}
