package com.planuz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;

import org.angmarch.views.NiceSpinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TeacherMainMenuActivity extends AppActivity {

    /**
     * Fills the spinners with data.
     */
    private void fillSpinners() {
        NiceSpinner instituteSpinner = findViewById(R.id.instituteSpinner);
        NiceSpinner teacherSpinner = findViewById(R.id.teacherSpinner);

        String[] institutes = new String[] {
                "Select course name",
                "Option 1",
                "Option 2",
                "Option 3"
        };
        List<String> instituteList = new ArrayList<>(Arrays.asList(institutes));

        String[] teachers = new String[] {
                "Select group name",
                "Option 1",
                "Option 2",
                "Option 3"
        };
        List<String> teacherList = new ArrayList<>(Arrays.asList(teachers));

        ArrayAdapter<String> instituteSpinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, instituteList) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }
        };

        instituteSpinner.setAdapter(instituteSpinnerArrayAdapter);

        ArrayAdapter<String> teacherSpinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, teacherList) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }
        };

        teacherSpinner.setAdapter(teacherSpinnerArrayAdapter);

    }

    /**
     * {inheritDoc}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_main_menu);
        setupActionBar();
        fillSpinners();
    }

    /**
     * Starts the student schedule activity.
     * @param view The element that caused this action.
     */
    public void showTeacherSchedule(View view) {
        Intent intent = new Intent(this, TeacherScheduleActivity.class);
        startActivity(intent);
    }
}
