package com.planuz;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import org.angmarch.views.NiceSpinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The configuration screen for the {@link TeacherScheduleWidget TeacherScheduleWidget} AppWidget.
 */
public class TeacherScheduleWidgetConfigureActivity extends Activity {

    private static final String PREFS_NAME = "com.planuz.TeacherScheduleWidget";
    private static final String PREF_PREFIX_KEY = "appwidget_";
    int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;

    NiceSpinner spinnerInstitute;
    NiceSpinner spinnerTeacher;

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            final Context context = TeacherScheduleWidgetConfigureActivity.this;


            String instituteText = spinnerInstitute.getText().toString(); /** Extract selection from spinners */
            String teacherText = spinnerTeacher.getText().toString();

            saveSelectionPref(context, mAppWidgetId, instituteText, teacherText); /** Save selection */

            // It is the responsibility of the configuration activity to update the app widget
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            TeacherScheduleWidget.updateAppWidget(context, appWidgetManager, mAppWidgetId);

            // Make sure we pass back the original appWidgetId
            Intent resultValue = new Intent();
            resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
            setResult(RESULT_OK, resultValue);
            finish();
        }
    };

    public TeacherScheduleWidgetConfigureActivity() {
        super();
    }


    /**
     * Saves selected preferences to SharedPreferences object for this widget
     */
    static void saveSelectionPref(Context context, int appWidgetId, String institute, String teacher) {
        SharedPreferences.Editor prefs = context.getSharedPreferences(PREFS_NAME, 0).edit();
        prefs.putString(PREF_PREFIX_KEY + "institute_" + appWidgetId, institute);
        prefs.putString(PREF_PREFIX_KEY + "teacher_" + appWidgetId, teacher);
        prefs.apply();
    }

    /**
     * Reads and returns saved institute selection from SharedPreferences object
     */
    static String loadInstitutePref(Context context, int appWidgetId) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        String prefValue = prefs.getString(PREF_PREFIX_KEY + "institute_" + appWidgetId, null);
        if (prefValue != null) {
            return prefValue;
        } else {
            return "error";
        }
    }

    /**
     * Reads and returns saved teacher selection from SharedPreferences object
     */
    static String loadTeacherPref(Context context, int appWidgetId) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        String prefValue = prefs.getString(PREF_PREFIX_KEY + "teacher_" + appWidgetId, null);
        if (prefValue != null) {
            return prefValue;
        } else {
            return "error";
        }
    }

    static void deleteSelectionPref(Context context, int appWidgetId) {
        SharedPreferences.Editor prefs = context.getSharedPreferences(PREFS_NAME, 0).edit();
        prefs.remove(PREF_PREFIX_KEY + "institute_" + appWidgetId);
        prefs.remove(PREF_PREFIX_KEY + "teacher_" + appWidgetId);
        prefs.apply();
    }


    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        // Set the result to CANCELED.  This will cause the widget host to cancel
        // out of the widget placement if the user presses the back button.
        setResult(RESULT_CANCELED);

        setContentView(R.layout.teacher_schedule_widget_configure);

        spinnerInstitute = (NiceSpinner) findViewById(R.id.instituteSpinnerConfigWT); /** Finds spinners */
        spinnerTeacher = (NiceSpinner) findViewById(R.id.teacherSpinnerConfigWT);

        findViewById(R.id.add_button).setOnClickListener(mOnClickListener);
        fillSpinners();

        // Find the widget id from the intent.
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mAppWidgetId = extras.getInt(
                    AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        // If this activity was started with an intent without an app widget ID, finish with an error.
        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
            return;
        }

    }

    /**
     * Fills the spinners with data.
     */
    private void fillSpinners() {
        NiceSpinner instituteSpinner = findViewById(R.id.instituteSpinnerConfigWT);
        NiceSpinner teacherSpinner = findViewById(R.id.teacherSpinnerConfigWT);

        String[] institutes = new String[] {
                "Select course name",
                "Option 1",
                "Option 2",
                "Option 3"
        };
        List<String> instituteList = new ArrayList<>(Arrays.asList(institutes));

        String[] teachers = new String[] {
                "Select group name",
                "Option 1",
                "Option 2",
                "Option 3"
        };
        List<String> teacherList = new ArrayList<>(Arrays.asList(teachers));

        ArrayAdapter<String> instituteSpinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, instituteList) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }
        };

        instituteSpinner.setAdapter(instituteSpinnerArrayAdapter);

        ArrayAdapter<String> teacherSpinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, teacherList) {
            @Override
            public boolean isEnabled(int position) {
                return position != 0;
            }
        };

        teacherSpinner.setAdapter(teacherSpinnerArrayAdapter);

    }

}

